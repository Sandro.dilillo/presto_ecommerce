<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RevisorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.revisor');
    }

    public function index()
    {
        $article = Article::where('is_accepted', null)
        ->orderBy('created_at','desc')
        ->first();
        return view('revisor.home', compact('article'));
    }

    private function setAccepted($article_id, $value){

        $article = Article::find($article_id);
        $article->is_accepted = $value;
        $article->save();
        return redirect(route('revisor.home'));
    }

    public function accept($article_id){

        return $this->setAccepted($article_id, true);
    }

    public function reject($article_id){
        $this->setAccepted($article_id, false);

        return redirect(route('revisor.home'));

        // return 
    }
    public function trash() {
        // $articles = Article::all();
        $articles = Article::where('is_accepted', false)
        ->orderBy('created_at','desc')
        ->get();
        return view('revisor.trash', compact('articles'));
    //     $this->setAccepted($article_id, false);
    }

    public function restore($article_id){

        return $this->setAccepted($article_id, true);
    }
}
