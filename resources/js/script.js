let navBar = document.querySelector('#navbar-presto')
document.addEventListener('scroll', () => {
    if(window.scrollY > 100) {
        navBar.classList.add('navbar-active')
    } else {
        navBar.classList.remove('navbar-active')
    }
})
let btnMenu = document.querySelector('.btn-menu')
btnMenu.addEventListener('click', () => {
    let iconMenu = document.querySelector('#icon-menu')
    let navBar = document.querySelector('#navbar-presto')
    iconMenu.classList.toggle('rotate')
    navBar.classList.toggle('navbar-active')
    
})

