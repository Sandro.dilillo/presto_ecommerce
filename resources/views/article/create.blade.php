<x-layout>
    <x-navbar>
        <div class="container d-flex my-5 justify-content-center px-5 ">
            {{-- @if ($errors->any())
                <div class=" my-5 alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}
        </div>
        <div class="container-fluid my-2 py-5 carrot-bg">
            <div class="row justify-content-center text-center p-3">
                
                <div class="col-12 col-md-8 card border-cstm shadow">
                    <div class="col-12 ">
                        <h3 class="tc-accent text-center pt-5 category-border"><span class="tc-sec "> {{__('ui.inserisci')}} </span>  {{__('ui.il tuo annuncio')}}</h3>
                      </div>
                    {{-- <div class="col-12 col-md-8 offset-0 offset-2"> --}}

                    <form method="POST" action="{{ route('article.store') }}" enctype="multipart/form-data"
                        class="text-center py-4 my-4 px-4">
                        {{-- <h3>Debug {{$uniqueSecret}}</h3> --}}
                        @csrf
                        <input type="hidden" name="uniqueSecret" value="{{ $uniqueSecret }}">

                        <div class="mb-3">
                            <label for="name" class="form-label">{{__('ui.nome articolo')}}</label>
                            <input type="text" name="name" class="form-control rounded-pill" id="name">
                        </div>
                        <div class="mb-3">
                            <label for="" class="form-label">{{__('ui.scegli')}}</label>
                            <select name="category" class="tc-sec custom-select my-select border mb-3">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ ucfirst($category->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">{{__('ui.descrizione')}}</label>
                            <textarea name="description" id="description" class="form-control rounded" cols="30"
                                rows="5"></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="price" class="form-label">{{__('ui.prezzo')}}</label>
                            <input type="text" name="price" class="form-control rounded-pill" id="price">
                        </div>
                        <div class="mb-3">
                            <label for="contact" class="form-label">{{__('ui.contatto')}} </label>
                            <input type="text" name="contact" class="form-control rounded-pill" id="contact"
                                aria-describedby="emailHelp" placeholder="Email">
                        </div>
                        <div class="mb-3">
                            {{-- <input type="file" name="img" class="form-control rounded-pill" id="img"> --}}

                            <label for="drophere" class="form-label"> {{__('ui.inserisci-i')}}</label>

                            <div class="dropzone" name="img" id="drophere"></div>
                        </div>

                        <button type="submit" class="btn btn-custom tc-base">{{__('ui.aggiungi articolo')}}</button>

                    </form>
                </div>
            </div>
        </div>













    </x-navbar>
</x-layout>
